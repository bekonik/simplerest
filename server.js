// Get the packages we need
var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var taskController = require('./controllers/task');
var passport = require('passport');
var Strategy = require('passport-http').DigestStrategy;
var db = require('./models');
var Task = require('./models/task');
// var authController = require('./controllers/auth');


passport.use(new Strategy({ qop: 'auth' },
  function(username, cb) {
    db.users.findByUsername(username, function(err, user) {
      if (err) { return cb(err); }
      if (!user) { return cb(null, false); }
      return cb(null, user, user.password);
    })
}));


mongoose.connect('mongodb://localhost:27017/restApp');


// Create our Express application
var app = express();

// Use the body-parser package in our application
app.use(bodyParser.urlencoded({
  extended: true
}));

app.use(passport.initialize());



var port = process.env.PORT || 3000;

// Create our Express router
var router = express.Router();

// router.get('/', function(req, res) {
//   res.json({ message: 'You are running dangerously low on beer!' });
// });
app.get('/tasks', passport.authenticate('digest', { session: false }), function(req, res) {
	Task.find(function(err, tasks) {    
    if (err)
      res.send(err);

    res.json(tasks);
  });	
});

app.post('/tasks', passport.authenticate('digest', { session: false }), function(req, res) {
	var task = new Task();

  // Set the beer properties that came from the POST data
  task.name = req.body.name;
  task.type = req.body.type;
  task.quantity = req.body.quantity;


  // Save the beer and check for errors
  task.save(function(err) {
    if (err)
      res.send(err);

    res.json({ message: 'Task added to the locker!', data: task });
  });
});

// router.route('/tasks')
//   .post(authController.isAuthenticated,taskController.postTasks)
//   .get(authController.isAuthenticated,taskController.getTasks);

//Create endpoint handlers for /beers/:beer_id
router.route('/tasks/:task_id')
  .get(taskController.getTask)
  .put(taskController.putTask)
  .delete(taskController.deleteTask);



// Register all our routes with /api
app.use('/api', router);

// Start the server
app.listen(port);
console.log('Insert beer on port ' + port);
