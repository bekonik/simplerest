// Load required packages
var Task = require('../models/task');

// Create endpoint /api/beers for POSTS
exports.postTasks = function(req, res) {
  // Create a new instance of the Beer model
  var task = new Task();

  // Set the beer properties that came from the POST data
  task.name = req.body.name;
  task.type = req.body.type;
  task.quantity = req.body.quantity;


  // Save the beer and check for errors
  task.save(function(err) {
    if (err)
      res.send(err);

    res.json({ message: 'Task added to the locker!', data: task });
  });
};

// Create endpoint /api/beers for GET
exports.getTasks = function(req, res) {
  // Use the Beer model to find all beer
  Task.find(function(err, tasks) {
    
    if (err)
      res.send(err);

    res.json(tasks);
  });
};

// Create endpoint /api/beers/:beer_id for GET
exports.getTask = function(req, res) {
  // Use the Beer model to find a specific beer
  Task.findById(req.params.task_id, function(err, task) {
    if (err)
      res.send(err);

    res.json(task);
  });
};

// Create endpoint /api/beers/:beer_id for PUT
exports.putTask = function(req, res) {
  // Use the Beer model to find a specific beer
  Task.findById(req.params.task_id, function(err, task) {
    if (err)
      res.send(err);
    task.quantity = req.body.quantity;
    task.save(function(err) {
      if (err)
        res.send(err);
      res.json(task);
    });
  });
};

// Create endpoint /api/beers/:beer_id for DELETE
exports.deleteTask = function(req, res) {
  // Use the Beer model to find a specific beer and remove it
  Task.findByIdAndRemove(req.params.task_id, function(err) {
    if (err)
      res.send(err);
    res.json({ message: 'Task removed from the locker!' });
  });
};
